import unittest

from common_primitives.k_neighbors_classifier import k_neighbors_classifier

from sklearn.model_selection import train_test_split
from sklearn import datasets

from random import randint
import numpy as np


class TestKNeighborsClassifier(unittest.TestCase):
    def test_fit(self):
        knc = k_neighbors_classifier.KNeighborsClassifier()

        iris = datasets.load_iris()
        X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.25, random_state=42)
        
        knc.set_training_data(inputs=X_train, outputs=y_train)
        knc.fit()

        sample_1 = X_test[randint(0,len(y_test)-1)]
        sample_2 = X_test[randint(0,len(y_test)-1)]
        assert np.array_equal(knc.produce(inputs=[sample_1, sample_2]),
                              knc.produce(inputs=np.vstack((sample_1, sample_2))))


if __name__ == '__main__':
    unittest.main()
