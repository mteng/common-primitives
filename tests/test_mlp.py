import unittest

import numpy as np

from common_primitives.mlp.mlp import MLP
from mnist import get_mnist_train_data


class MLPTestCase(unittest.TestCase):
    def test_regression_xor(self):
        """
        MLP train (fit) and test (produce) expected resuts with XOR
        """

        # These particular values are picked because they result in a test that concludes quickly
        mlp = MLP(depth=4, width=64, activation='tanh')
        xor_inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
        xor_outputs = np.array([[0], [1], [1], [0]])
        mlp.set_training_data(inputs=xor_inputs, outputs=xor_outputs)
        mlp.fit(iterations=10000)
        outputs = mlp.produce(inputs=xor_inputs)
        self.assertTrue(np.allclose(outputs, xor_outputs, 0.1, 0.1))

    def test_classification_mnist(self):
        """
        MLP train (fit) and test (produce) expected resuts with MNIST
        """
        mlp = MLP(depth=4, width=64, activation='relu', loss='crossentropy', output_shape=(10,))
        mnist_train_inputs, mnist_train_outputs = get_mnist_train_data(10)
        mlp.set_training_data(inputs=mnist_train_inputs, outputs=mnist_train_outputs)
        mlp.fit(iterations=10000)
        outputs = mlp.produce(inputs=mnist_train_inputs)
        outputs = np.argmax(outputs,1)
        accuracy = np.sum(outputs == mnist_train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)

    def test_get_set_params(self):
        """
        MLP get and set params
        """

        mlp = MLP(depth=4, width=64, activation='tanh')
        xor_inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
        xor_outputs = np.array([[0], [1], [1], [0]])
        mlp.set_training_data(inputs=xor_inputs, outputs=xor_outputs)
        params = mlp.get_params()
        mlp.set_params(params=params)
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
