import unittest

import os

from common_primitives.csv_feature_extractor import csv_feature_extractor


class TestCSVFeatureExtractor(unittest.TestCase):
    def test_extraction(self):
        extractor = csv_feature_extractor.CSVFeatureExtractor()

        TEST_FILE = os.path.dirname(os.path.realpath(__file__)) + "/test_data_csv/test.csv"
        datas = extractor.produce(inputs=[TEST_FILE])
        data = datas[0]

        # should be {'word': ['one', 'two', 'three'], 'number': [1, 2, 3]}
        assert data["word"][2] == "three"
        assert type(data["number"][0]) == int


if __name__ == '__main__':
    unittest.main()
