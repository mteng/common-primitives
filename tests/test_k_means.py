import unittest

import numpy as np

from common_primitives.k_means.k_means import KMeans


class KMeansTestCase(unittest.TestCase):
    def test_fit_produce(self):
        """
        KMeans fit and produce results.
        """

        a = KMeans()
        train_x = np.array([[2, 8], [3.5, 3], [7, 2], [8, 1], [9, 13], [1, 2], [5, 7], [1, 3]])
        a.set_training_data(inputs=train_x)
        a.fit()
        # You can use a list of multiple samples.
        result = a.produce(inputs=[np.array([3, 3]), np.array([13, 13])])

        # Or you can use one array which has multiple samples along the first dimension.
        result = a.produce(inputs=np.array([[3, 3], [13, 13]]))
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
