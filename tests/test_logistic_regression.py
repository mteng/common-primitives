import unittest

import numpy as np

from common_primitives.logistic_regression import logistic_regression


class TestLogisticRegression(unittest.TestCase):
    def test_fit_sample_produce(self, burnin=200):
        a = logistic_regression.BayesianLogisticRegression()
        num_features = 3
        num_rows = 10

        train_x = np.random.randn(num_rows, num_features)
        train_y = np.random.binomial(1, 0.5, num_rows)

        print(train_x, train_x.shape, train_y, train_y.shape)
        a.set_training_data(inputs=train_x, outputs=train_y)

        # this takes a few seconds
        a.fit()
        # just test that it gives right types
        test_x = np.random.randn(5, num_features)
        assert type(a.produce(inputs=test_x)[0]) == int or\
            type(a.produce(inputs=test_x)[0]) == np.int64
        assert type(a.sample(inputs=test_x, num_samples=7)[0][0]) == int or\
            type(a.sample(inputs=test_x, num_samples=7)[0][0]) == np.int64
        assert type(a.log_likelihood(outputs=train_y)) == float

        # check shape of sample
        # print(a.sample(inputs=test_x, num_samples=2), (2, 5))
        assert np.array_equal(a.sample(inputs=test_x, num_samples=2).shape, (2, 5))

    def test_fit_sample_loglikelihood(self, burnin=200):
        a = logistic_regression.BayesianLogisticRegression()
        num_features = 3
        num_rows = 10

        train_x = np.random.randn(num_rows, num_features)
        train_y = np.random.binomial(1, 0.5, num_rows)
        a.set_training_data(inputs=train_x, outputs=train_y)

        # this takes a few seconds
        a.fit()

        # just test that it gives right types
        assert a.log_likelihood(outputs=train_y,
                                inputs=train_x) ==\
            a.log_likelihood(outputs=train_y)


if __name__ == '__main__':
    unittest.main()
