import unittest

import numpy as np

from common_primitives.cnn.cnn import CNN
from mnist import get_mnist_train_data


class CNNTestCase(unittest.TestCase):
    def test_classification_mnist(self):
        """
        CNN train (fit) and test (produce) expected resuts with MNIST
        """
        cnn = CNN(output_shape=(10,))
        mnist_train_inputs, mnist_train_outputs = get_mnist_train_data(10)
        cnn.set_training_data(inputs=mnist_train_inputs, outputs=mnist_train_outputs)
        cnn.fit(iterations=10000)
        outputs = cnn.produce(inputs=mnist_train_inputs)
        outputs = np.argmax(outputs,1)
        accuracy = np.sum(outputs == mnist_train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)


if __name__ == '__main__':
    unittest.main()
