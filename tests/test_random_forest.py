import unittest

import numpy as np

from common_primitives.random_forest import random_forest

class TestRandomForest(unittest.TestCase):
    def test_fit(self):
        a = random_forest.RandomForest()
        train_y = np.array(['a', 'b', 'a', 'b', 'a'])
        train_x = np.array([[2, 8], [3.5, 3], [7, 2], [8, 1], [9, 13]])
        a.set_training_data(inputs=train_x, outputs=train_y)
        a.fit()

        assert np.array_equal(a.produce(inputs=[np.array([3, 3]), np.array([13, 13])]),
                              a.produce(inputs=np.array([[3, 3], [13, 13]])))


if __name__ == '__main__':
    unittest.main()
