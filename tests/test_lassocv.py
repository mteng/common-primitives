import unittest

from common_primitives.lassoCV import lassoCV

from sklearn.model_selection import train_test_split
from sklearn import datasets

from random import randint
import numpy as np


class TestLassoCV(unittest.TestCase):
    def test_fit(self):
        lcv = lassoCV.LassoCV()

        boston = datasets.load_boston()
        X_train, X_test, y_train, y_test = train_test_split(boston.data, boston.target, test_size=0.25, random_state=42)
        
        lcv.set_training_data(inputs=X_train, outputs=y_train)
        lcv.fit()

        sample_1 = X_test[randint(0,len(y_test)-1)]
        sample_2 = X_test[randint(0,len(y_test)-1)]
        assert np.array_equal(lcv.produce(inputs=[sample_1, sample_2]),
                              lcv.produce(inputs=np.vstack((sample_1, sample_2))))


if __name__ == '__main__':
    unittest.main()
