import unittest

import numpy as np
from numpy import testing

from common_primitives.pca import pca


class TestPCA(unittest.TestCase):
    def test_fit(self):
        a = pca.PCA(max_components=2, proportion_variance=1.01)    # so it will give us even components that are zero
        a.set_training_data(inputs=np.array([[1, 2, 3, 4],
                                            [1, 2, 3, 4],
                                            [1, 2, 3, 4],
                                            [1, 1, 3, 4],
                                            [1, 3, 3, 4]]))
        a.fit()

        # feed in 2 4D data points which should be transformed to 2 2D points
        transformed = a.produce(inputs=np.array([[1, 2, 3, 4],
                                                 [1, 10, 3, 4]]))

        self.assertTrue(transformed.shape == (2, 2))    # check shape is correct
        testing.assert_array_equal(transformed[0], np.zeros(2))  # check first data point is at origin
        self.assertTrue(np.linalg.norm(transformed) == 8)  # check second data point isn't at origin

        params = a.get_params()
        self.assertTrue(np.linalg.norm(params.transformation[0]) == 1)  # check rows are unit vectors


if __name__ == '__main__':
    unittest.main()
