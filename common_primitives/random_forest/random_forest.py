from typing import NamedTuple, Union, List, Sequence, Any

from sklearn.tree import DecisionTreeClassifier  # type: ignore
from sklearn.ensemble import RandomForestClassifier  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = Sequence[Any]

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('estimators', List[DecisionTreeClassifier]),
])


class RandomForest(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Example of a primitive wrapping an existing sklearn classifier.
    """

    __author__ = "Oxford DARPA D3M \"Hasty\" team"
    __metadata__ = {
        "common_name": "Random Forest",
        "algorithm_type": ["Ensemble", "Decision Tree"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": True,
        "handles_multilabel": True,
        "input_type": ["DENSE", "UNISGNED_DATA"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 n_estimators: int = 10,
                 criterion: str = 'gini',
                 max_features: Union[int, str] = 'auto',
                 max_depth: int = None,
                 min_samples_split: int = 2,
                 min_samples_leaf: int = 1,
                 min_weight_fraction_leaf: float = 0.,
                 max_leaf_nodes: int = None,
                 min_impurity_decrease: float = 0.,
                 oob_score: bool = False) -> None:

        super().__init__()

        self.random_forest = RandomForestClassifier(
            n_estimators=n_estimators,
            criterion=criterion,
            max_features=max_features,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            oob_score=oob_score)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: Sequence[Any]
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.random_forest.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.random_forest.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(estimators=self.random_forest.estimators_)

    def set_params(self, *, params: Params) -> None:
        self.random_forest.estimators_ = params.estimators
