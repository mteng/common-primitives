from typing import NamedTuple, Union

from sklearn.cluster import KMeans as KMeans_  # type: ignore


from d3m_metadata.sequence import ndarray
from primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('cluster_centers', ndarray),  # Coordinates of cluster centers.
])


class KMeans(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Example of a primitive wrapping an existing sklearn clustering algorithm.
    """

    __author__ = 'Oxford DARPA D3M "Hasty" team'
    __metadata__ = {
        "common_name": "K-means Clustering",
        "algorithm_type": ["Instance Based"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": [],
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": False,
        "handles_multilabel": False,
        "input_type": ["DENSE", "UNISGNED_DATA"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 n_clusters: int = 8,
                 init: Union[str, ndarray] = 'k-means++',
                 n_init: int = 10,
                 max_iter: int = 300) -> None:

        super().__init__()

        # TODO: This could be a nice example to demonstrate iterative fitting.
        #       Instead of having to provide max_iter in advance.
        self.kmeans = KMeans_(n_clusters=n_clusters,
                              init=init,
                              n_init=n_init,
                              max_iter=max_iter)
        self.training_inputs = None  # type: Inputs
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.kmeans.predict(inputs)

    def set_training_data(self, *, inputs: Inputs) -> None:
        self.training_inputs = inputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None:
            raise ValueError("Missing training data.")

        self.kmeans.fit(self.training_inputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(cluster_centers=self.kmeans.cluster_centers_)

    def set_params(self, *, params: Params) -> None:
        self.kmeans.cluster_centers_ = params.cluster_centers
