# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
__version__ = '2017.10.10rc0'
__author__ = 'D3M project'
__metadata__ = {'languages': ['python3.6', 'python3.5', 'python2.7'], 'library': 'common_primitives', 'source_code': 'https://gitlab.com/datadrivendiscovery/common-primitives',
                'build': [{'type': 'pip', 'package': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git'}]}
