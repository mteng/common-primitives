# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import time
from typing import NamedTuple, Sequence, Tuple, Optional
import numpy as np
import torch
from d3m_metadata.sequence import List, ndarray
from primitive_interfaces.base import ProbabilisticCompositionalityMixin, GradientCompositionalityMixin, SamplingCompositionalityMixin, CallMetadata
from primitive_interfaces.generator import GeneratorPrimitiveBase
from ..helpers import to_variable, refresh_node, log_mvn_likelihood
Void = type(None)
Inputs = List[Void]
Outputs = ndarray
Params = NamedTuple('Params', [('mean', ndarray), ('covariance', ndarray)])
Scores = NamedTuple('Scores', [('mean', ndarray), ('covariance', ndarray)])
Gradients = ndarray


class DiagonalMVN(ProbabilisticCompositionalityMixin[(Inputs, Outputs, Params)], GradientCompositionalityMixin[(Inputs, Outputs, Params)], SamplingCompositionalityMixin[(Inputs, Outputs, Params)], GeneratorPrimitiveBase[(Outputs, Params)]):
    '\n    Example of a primitive which allows fitting, and sampling from,\n    a multivariate Gaussian (with diagonal covariance matrix)\n    '
    __author__ = 'William Harvey <willh@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Diagonal Multi-variate Normal', 'handles_regression': True, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False, 'algorithm_type': ['Gradient Descent'], 'compute_resources': {
        'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'task_type': ['Modeling'], 'learning_type': ['Unsupervised learning']}

    def __init__(self, alpha=0.01, beta=1e-08):
        # type: (float, float) -> None
        "\n        Optionally set ``alpha``, the initial fitting step size,\n        and ``beta``, which is as described in Baydin, Atilim Gunes, et al.,\n        'Online Learning Rate Adaptation with Hypergradient Descent',\n        arXiv preprint arXiv:1703.04782 (2017).\n        "
        super(DiagonalMVN, self).__init__()
        self.alpha = alpha
        self.beta = beta
        self.fit_term_temperature = 0.0
        self.mean = None  # type: torch.autograd.Variable
        self.covariance = None  # type: torch.autograd.Variable
        self.training_outputs = None  # type: torch.autograd.Variable
        self.new_training_outputs = True
        self.fitted = True
        self.iterations_done = None  # type: Optional[int]

    def _produce_one(self):
        # type: () -> ndarray
        return self.mean.data.numpy()

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        self.fitted = True
        self.iterations_done = None
        return np.array([self._produce_one() for _ in inputs])

    def set_training_data(self, outputs):
        # type: (Outputs) -> None
        self.training_outputs = to_variable(outputs, requires_grad=True)
        self.new_training_outputs = True

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        '\n        Fits parameters to MLE. Runs gradient descent for ``timeout`` seconds or ``iterations``\n        iterations, whichever comes sooner, on log likelihood of training data.\n        '
        if (self.training_outputs is None):
            raise ValueError('Missing training data.')
        if (timeout is None):
            timeout = np.inf
        if (iterations is None):
            iterations = 100
        if self.new_training_outputs:
            self.mean = to_variable(
                np.zeros(self.training_outputs.size()[0]), True)
            self.covariance = to_variable(
                np.eye(self.training_outputs.size()[0]), True)
            self.new_training_outputs = False
        start = time.time()
        self.fitted = False
        self.iterations_done = 0
        (prev_mean_grad, prev_covariance_grad) = (
            None, None)  # type: torch.Tensor, torch.Tensor
        while ((time.time() < (start + timeout)) and (self.iterations_done < iterations)):
            self.iterations_done += 1
            log_likelihood = sum((self._log_likelihood(
                output=training_output) for training_output in self.training_outputs))
            self.mean.retain_grad()
            self.covariance.retain_grad()
            log_likelihood.backward()
            mean_grad = self.mean.grad.data
            covariance_grad = self.covariance.grad.data
            if (prev_mean_grad is not None):
                self.alpha += (self.beta * (torch.dot(mean_grad, prev_mean_grad) + torch.dot(
                    covariance_grad.view((- 1)), prev_covariance_grad.view((- 1)))))
            (prev_mean_grad, prev_covariance_grad) = (
                mean_grad, covariance_grad)
            self.mean.data += ((mean_grad * self.alpha) /
                               torch.norm(mean_grad))
            self.covariance.data += ((covariance_grad *
                                      self.alpha) / torch.norm(prev_covariance_grad))
            self.mean = refresh_node(self.mean)
            self.covariance = refresh_node(self.covariance)

    def get_params(self):
        # type: () -> Params
        return Params(mean=self.mean.data.numpy(), covariance=self.covariance.data.numpy())

    def set_params(self, params):
        # type: (Params) -> None
        self.mean = to_variable(params.mean, requires_grad=True)
        self.covariance = to_variable(params.covariance, requires_grad=True)

    def _sample_once(self, inputs):
        # type: (Inputs) -> Outputs
        mean = self.mean.data.numpy()
        covariance = self.covariance.data.numpy()
        return np.array([np.random.multivariate_normal(mean, covariance) for _ in inputs])

    def sample(self, inputs, num_samples=1, timeout=None, iterations=None):
        # type: (Inputs, int, float, int) -> Sequence[Outputs]
        return np.array([self._sample_once(inputs=inputs) for _ in range(num_samples)])

    def _log_likelihood(self, output):
        # type: (torch.autograd.Variable) -> torch.autograd.Variable
        '\n        Calculates log(normal_density(self.mean, self.covariance)).\n        '
        output = to_variable(output)
        return log_mvn_likelihood(self.mean, self.covariance, output)

    def _gradient_output_log_likelihood(self, output):
        # type: (torch.autograd.Variable) -> torch.autograd.Variable
        '\n        Output is D-length torch variable.\n        '
        output = refresh_node(output)
        log_likelihood = self._log_likelihood(output=output)
        log_likelihood.backward()
        return output.grad

    def _gradient_params_log_likelihood(self, output):
        # type: (torch.autograd.Variable) -> Tuple[(torch.autograd.Variable, torch.autograd.Variable)]
        '\n        Output is D-length torch variable.\n        '
        self.mean = refresh_node(self.mean)
        self.covariance = refresh_node(self.covariance)
        log_likelihood = self._log_likelihood(output=output)
        log_likelihood.backward()
        return (self.mean.grad, self.covariance.grad)

    def log_likelihood(self, outputs, inputs=None):
        # type: (Outputs, Inputs) -> float
        '\n        Calculates log(normal_density(self.mean, self.covariance)).\n        '
        return float(sum((self._log_likelihood(output=output).data.numpy() for output in outputs))[0])

    def gradient_output(self, outputs, inputs=None):
        # type: (Outputs, Inputs) -> Gradients
        '\n        Calculates gradient of log(normal_density(self.mean, self.covariance)) * fit_term_temperature with respect to output.\n        '
        outputs_vars = [to_variable(output, requires_grad=True)
                        for output in outputs]
        grad = sum((self._gradient_output_log_likelihood(output=output)
                    for output in outputs_vars))
        if (self.fit_term_temperature != 0):
            grad += (self.fit_term_temperature * sum((self._gradient_output_log_likelihood(
                output=training_output) for training_output in self.training_outputs)))
        return grad.data.numpy()

    def gradient_params(self, outputs, inputs=None):
        # type: (Outputs, Inputs) -> Scores
        '\n        Calculates gradient of log(normal_density(self.mean, self.covariance)) * fit_term_temperature with respect to params.\n        '
        outputs_vars = [to_variable(output, requires_grad=True)
                        for output in outputs]
        grads = [self._gradient_params_log_likelihood(
            output=output) for output in outputs_vars]
        grad_mean = sum((grad[0] for grad in grads))
        grad_covariance = sum((grad[1] for grad in grads))
        if (self.fit_term_temperature != 0):
            training_grads = [self._gradient_params_log_likelihood(
                output=training_output) for training_output in self.training_outputs]
            grad_mean += (self.fit_term_temperature *
                          sum((grad[0] for grad in training_grads)))
            grad_covariance += (self.fit_term_temperature *
                                sum((grad[1] for grad in training_grads)))
        return Scores(mean=grad_mean.data.numpy(), covariance=grad_covariance.data.numpy())

    def set_fit_term_temperature(self, temperature=0):
        # type: (float) -> None
        self.fit_term_temperature = temperature

    def get_call_metadata(self):
        # type: () -> CallMetadata
        return CallMetadata(has_finished=False, iterations_done=self.iterations_done)
