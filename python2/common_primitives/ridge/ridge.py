# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple
from sklearn.linear_model import Ridge as RR
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('coefficient', ndarray)])


class Ridge(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Primitive wrapping for sklearn.linear_model.Ridge.\n    '
    __author__ = 'NYU DARPA D3M team'
    __metadata__ = {'common_name': 'Ridge Regression', 'algorithm_type': ['Regression'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': True, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False, 'input_type': ['DENSE', 'SPARSE'], 'output_type': ['PREDICTIONS']}

    def __init__(self, alpha=1.0, fit_intercept=True, normalize=False, max_iter=None, tol=0.001, solver='auto'):
        # type: (float, bool, bool, int, float, str) -> None
        super(Ridge, self).__init__()
        self.ridge = RR(alpha=alpha, fit_intercept=fit_intercept, normalize=normalize,
                        copy_X=True, max_iter=max_iter, tol=tol, solver=solver)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.ridge.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.ridge.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(coefficient=self.ridge.coef_)

    def set_params(self, params):
        # type: (Params) -> None
        self.ridge.coef_ = params.coefficient
