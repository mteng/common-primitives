# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import abc
import time
from typing import NamedTuple, Dict, Any, Optional, Tuple
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from d3m_metadata.sequence import ndarray
from primitive_interfaces.base import CallMetadata
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from .helpers import to_variable
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('state', Dict[(unicode, ndarray)])])


class NNPrimitive(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    __author__ = 'Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Neural network base primitive for PyTorch neural networks', 'algorithm_type': ['Deep Learning'], 'compute_resources': {
        'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}}

    def __init__(self, loss='mse', output_shape=None, linear_output=False):
        # type: (str, Tuple[(int, ...)], bool) -> None
        super(NNPrimitive, self).__init__()
        self._net = None  # type: Any
        if (loss == 'mse'):
            self._criterion = nn.MSELoss()
            self._linear_output = linear_output
        elif (loss == 'crossentropy'):
            self._criterion = nn.CrossEntropyLoss()
            self._linear_output = True
        else:
            raise ValueError(
                'Unsupported loss: {}. Available options: mse, crossentropy'.format(loss))
        self._output_shape = output_shape
        self._inputs = None  # type: Optional[torch.Variable]
        self._outputs = None  # type: Optional[torch.Variable]
        self._training_inputs = None  # type: Optional[torch.Variable]
        self._training_outputs = None  # type: Optional[torch.Variable]
        self._training_size = None  # type: Optional[int]
        self._iterations_done = None  # type: Optional[int]
        self._has_finished = True

    @abc.abstractmethod
    def _init_net(self):
        # type: () -> None
        '\n        Initialize the internal neural network _net\n        '

    def _get_params(self, state_dict):
        # type: () -> Params
        return Params(state={k: v.numpy() for (k, v) in state_dict.items()})

    def get_params(self):
        # type: () -> Params
        return self._get_params(self._net.state_dict())

    def set_params(self, params):
        # type: (Params) -> None
        state = self._net.state_dict()
        new_state = {k: torch.from_numpy(v) for (k, v) in params.state.items()}
        state.update(new_state)
        self._net.load_state_dict(state)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        if (len(inputs) != len(outputs)):
            raise ValueError(
                'Training data sequences "inputs" and "outputs" should have the same length.')
        self._training_size = len(inputs)
        self._training_inputs = to_variable(inputs)
        self._training_outputs = to_variable(outputs)
        if (self._training_inputs.dim() == 1):
            self._training_inputs = self._training_inputs.unsqueeze(0)
        self._input_shape = self._training_inputs[0].data.numpy().shape
        if isinstance(self._criterion, nn.CrossEntropyLoss):
            self._training_outputs = self._training_outputs.type(
                torch.LongTensor)
            if (self._output_shape is None):
                self._output_shape = (1,)
        else:
            if (self._training_outputs.dim() == 1):
                self._training_outputs = self._training_outputs.unsqueeze(1)
            if (self._output_shape is None):
                self._output_shape = self._training_outputs[0].data.numpy(
                ).shape
        self._init_net()

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        if (self._net is None):
            raise Exception(
                'Neural network not initialized. You need to set training data so that the network structure can be defined.')
        self._iterations_done = None
        self._has_finished = True
        self._net.eval()
        self._inputs = to_variable(inputs, requires_grad=True)
        self._outputs = self._net.forward(self._inputs)
        return self._outputs.data.numpy()

    def fit(self, timeout=None, iterations=None, minibatch_size=64, learning_rate=0.0001, momentum=0.9, weight_decay=1e-05, optimizer='adam', shuffle=True, fit_threshold=0.001):
        # type: (float, int, int, float, float, float, str, bool, float) -> None
        if (self._training_inputs is None):
            raise Exception('Cannot fit when no training data is present.')
        if (timeout is None):
            timeout = np.inf
        if (iterations is None):
            iterations = 100
        if (minibatch_size > self._training_size):
            minibatch_size = self._training_size
        if (optimizer == 'adam'):
            optimizer_instance = optim.Adam(
                self._net.parameters(), lr=learning_rate, weight_decay=weight_decay)
        elif (optimizer == 'sgd'):
            optimizer_instance = optim.SGD(self._net.parameters(
            ), lr=learning_rate, momentum=momentum, weight_decay=weight_decay)
        else:
            raise ValueError(
                'Unsupported optimizer: {}. Available options: adam, sgd'.format(optimizer))
        start = time.time()
        self._iterations_done = 0
        self._has_finished = False
        self._net.train()
        while ((time.time() < (start + timeout)) and (self._iterations_done < iterations)):
            self._iterations_done += 1
            if shuffle:
                permute = torch.randperm(self._training_size)
                self._training_inputs = self._training_inputs[permute]
                self._training_outputs = self._training_outputs[permute]
            for i in range(0, self._training_size, minibatch_size):
                i_end = min((i + minibatch_size), self._training_size)
                minibatch_inputs = self._training_inputs[i:i_end]
                minibatch_outputs = self._training_outputs[i:i_end]
                optimizer_instance.zero_grad()
                o = self._net(minibatch_inputs)
                loss = self._criterion(o, minibatch_outputs)
                loss.backward()
                optimizer_instance.step()
                if (loss.data[0] < fit_threshold):
                    self._has_finished = True
                    return

    def backprop(self, gradient_outputs, fine_tune=False, learning_rate=1e-05, weight_decay=1e-05):
        # type: (Outputs, bool, float, float) -> Tuple[(Inputs, Params)]
        if (self._inputs is None):
            raise Exception(
                'Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            optimizer_instance = optim.SGD(
                self._net.parameters(), lr=learning_rate, weight_decay=weight_decay)
            if (self._inputs.grad is not None):
                self._inputs.grad.data.zero_()
            optimizer_instance.zero_grad()
            self._outputs.backward(gradient=to_variable(gradient_outputs))
            if fine_tune:
                optimizer_instance.step()
            grad_inputs = self._inputs.grad
            grad_params_state_dict = {k: v.clone().fill_(
                0) for (k, v) in self._net.state_dict().items()}
            named_parameters = {k: v.grad.data for (
                k, v) in self._net.named_parameters()}
            grad_params_state_dict = {k: (named_parameters[k] if (
                k in named_parameters) else v) for (k, v) in grad_params_state_dict.items()}
            grad_params = self._get_params(grad_params_state_dict)
            return (grad_inputs, grad_params)

    def gradient_output(self):
        raise NotImplementedError()

    def gradient_params(self):
        raise NotImplementedError()

    def set_fit_term_temperature(self):
        raise NotImplementedError()

    def get_call_metadata(self):
        # type: () -> CallMetadata
        return CallMetadata(has_finished=self._has_finished, iterations_done=self._iterations_done)
