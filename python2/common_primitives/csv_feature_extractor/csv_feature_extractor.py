# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import pandas as pd
from d3m_metadata.sequence import List
from primitive_interfaces.transformer import TransformerPrimitiveBase
from ..helpers import denumpify
Inputs = List[unicode]
Outputs = List[dict]


class CSVFeatureExtractor(TransformerPrimitiveBase[(Inputs, Outputs)]):
    '\n    Primitive which takes a path to a CSV file of named columns and returns\n    a dictionary of {"<heading1>": [ <column_1_values> ]}.\n    '
    __author__ = 'William Harvey <willh@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'CSV Feature Extractor', 'algorithm_type': ['Data Processing'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [
    ], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': False, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False}

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return List([self._produce_one(input=i) for i in inputs])

    def _produce_one(self, input):
        # type: (str) -> dict
        data = pd.read_csv(input)
        return {key: [denumpify(i) for i in data[key]] for key in data}
