# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple, Sequence
import numpy as np
from pymc3 import Model, Normal, Bernoulli, NUTS
from pymc3 import invlogit, sample, sample_ppc
import pymc3 as pm
from pymc3.backends.base import MultiTrace
import theano
from d3m_metadata.sequence import ndarray
from primitive_interfaces.base import ProbabilisticCompositionalityMixin, SamplingCompositionalityMixin
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Input = ndarray
Output = int
Params = NamedTuple('Params', [('weights', MultiTrace)])


class BayesianLogisticRegression(ProbabilisticCompositionalityMixin[(Inputs, Outputs, Params)], SamplingCompositionalityMixin[(Inputs, Outputs, Params)], SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Example of a primitive wrapping logistic regression using PyMC3 and its\n    Theano backend.\n    '
    __author__ = 'Oxford DARPA D3M "Hasty" team'
    __metadata__ = {'common_name': 'Logistic Regression', 'algorithm_type': ['Gradient Descent'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': True, 'handles_classification': True, 'handles_multiclass': True, 'handles_multilabel': False, 'input_type': ['DENSE', 'UNISGNED_DATA'], 'output_type': ['PREDICTIONS']}

    def __init__(self, burnin=1000):
        # type: (int) -> None
        super(BayesianLogisticRegression, self).__init__()
        self.burnin = burnin
        self.seed = (- 1)
        self.fitted = False
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.trace = None  # type: MultiTrace
        self.model = None  # type: Model

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        '\n        Sample a Bayesian Logistic Regression model using NUTS to find\n        some reasonably weights\n        '
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        (_, n_features) = self.training_inputs.shape
        self.training_inputs = theano.shared(self.training_inputs)
        self.training_outputs = theano.shared(self.training_outputs)
        with Model() as model:
            weights = Normal('weights', 0, 1, shape=n_features)
            p = invlogit(pm.math.dot(self.training_inputs, weights))
            Bernoulli('y', p, observed=self.training_outputs)
            step = NUTS()
            trace = sample(1000, step, random_seed=self.seed,
                           tune=self.burnin, progressbar=False)
        self.model = model
        self.trace = trace
        self.fitted = True

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        self.training_inputs.set_value(inputs)
        self.training_outputs.set_value(
            np.random.binomial(1, 0.5, inputs.shape[0]))
        with self.model:
            post_pred = sample_ppc(self.trace, samples=50, progressbar=False)
        return np.random.binomial(1, post_pred['y'].mean(axis=0)).astype(int)

    def sample(self, inputs, num_samples=1, timeout=None, iterations=None):
        # type: (Inputs, int, float, int) -> Sequence[Outputs]
        self.training_inputs.set_value(inputs)
        self.training_outputs.set_value(
            np.random.binomial(1, 0.5, inputs.shape[0]))
        with self.model:
            post_pred = sample_ppc(
                self.trace, samples=num_samples, progressbar=False)
        return post_pred['y'].astype(int)

    def _log_likelihood(self, output):
        # type: (Output) -> float
        'Provides a likelihood of one input/output pair given the weights'
        logp = self.model.logp
        weights = self.trace['weights']
        return float(np.array([logp(dict(y=output, weights=w)) for w in weights]).mean())

    def log_likelihood(self, outputs, inputs=None):
        # type: (Outputs, Inputs) -> float
        'Provides a likelihood of the data given the weights'
        return sum((self._log_likelihood(output=output) for output in outputs))

    def get_params(self):
        # type: () -> Params
        return Params(weights=self.trace)

    def set_params(self, params):
        # type: (Params) -> None
        self.trace = params.weights

    def set_random_seed(self, seed):
        # type: (int) -> None
        self.seed = seed
