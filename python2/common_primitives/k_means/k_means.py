# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple, Union
from sklearn.cluster import KMeans as KMeans_
from d3m_metadata.sequence import ndarray
from primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('cluster_centers', ndarray)])


class KMeans(UnsupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Example of a primitive wrapping an existing sklearn clustering algorithm.\n    '
    __author__ = 'Oxford DARPA D3M "Hasty" team'
    __metadata__ = {'common_name': 'K-means Clustering', 'algorithm_type': ['Instance Based'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': False, 'handles_classification': True, 'handles_multiclass': False, 'handles_multilabel': False, 'input_type': ['DENSE', 'UNISGNED_DATA'], 'output_type': ['PREDICTIONS']}

    def __init__(self, n_clusters=8, init='k-means++', n_init=10, max_iter=300):
        # type: (int, Union[(str, ndarray)], int, int) -> None
        super(KMeans, self).__init__()
        self.kmeans = KMeans_(n_clusters=n_clusters,
                              init=init, n_init=n_init, max_iter=max_iter)
        self.training_inputs = None  # type: Inputs
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.kmeans.predict(inputs)

    def set_training_data(self, inputs):
        # type: (Inputs) -> None
        self.training_inputs = inputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if (self.training_inputs is None):
            raise ValueError('Missing training data.')
        self.kmeans.fit(self.training_inputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(cluster_centers=self.kmeans.cluster_centers_)

    def set_params(self, params):
        # type: (Params) -> None
        self.kmeans.cluster_centers_ = params.cluster_centers
