# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple
from sklearn.neighbors import KNeighborsClassifier as KNC
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('instances', ndarray), ('targets', ndarray)])


class KNeighborsClassifier(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Primitive wrapping for sklearn.neighbors.KNeigborsClassifier.\n    '
    __author__ = 'NYU DARPA D3M team'
    __metadata__ = {'common_name': 'K Neighbors Classification', 'algorithm_type': ['Instance Based', 'Classification'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': False, 'handles_classification': True, 'handles_multiclass': True, 'handles_multilabel': False, 'input_type': ['DENSE', 'SPARSE'], 'output_type': ['PREDICTIONS']}

    def __init__(self, n_neighbors=5, weights='uniform', algorithm='auto', leaf_size=30, p=2, metric='minkowski', metric_params=None):
        # type: (int, str, str, int, int, str, dict) -> None
        super(KNeighborsClassifier, self).__init__()
        self.knc = KNC(n_neighbors=n_neighbors, weights=weights, algorithm=algorithm,
                       leaf_size=leaf_size, p=p, metric=metric, metric_params=metric_params)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.knc.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.knc.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(instances=self.training_inputs, targets=self.training_outputs)

    def set_params(self, params):
        # type: (Params) -> None
        self.training_inputs = Params.instances
        self.training_outputs = Params.targets
