# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple, Union
from sklearn.linear_model import LassoCV as LCV
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('coefficient', ndarray)])


class LassoCV(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Primitive wrapping for sklearn.linear_model.LassoCV.\n    '
    __author__ = 'NYU DARPA D3M team'
    __metadata__ = {'common_name': 'Lasso CV', 'algorithm_type': ['Regression'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [
    ], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': True, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False, 'input_type': ['DENSE', 'SPARSE'], 'output_type': ['PREDICTIONS']}

    def __init__(self, eps=0.001, n_alphas=100, alphas=None, fit_intercept=True, normalize=False, precompute='auto', max_iter=1000, tol=0.0001, copy_X=True, cv=None, _verbose=False, positive=False, selection='cyclic'):
        # type: (float, int, ndarray, bool, bool, Union[(bool, str, ndarray)], int, float, bool, int, bool, bool, str) -> None
        super(LassoCV, self).__init__()
        self.lassocv = LCV(eps=eps, n_alphas=n_alphas, alphas=alphas, fit_intercept=fit_intercept, normalize=normalize, precompute=precompute,
                           max_iter=max_iter, tol=tol, copy_X=copy_X, cv=cv, verbose=_verbose, positive=positive, selection=selection)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.lassocv.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.lassocv.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(coefficient=self.lassocv.coef_)

    def set_params(self, params):
        # type: (Params) -> None
        self.lassocv.coef_ = params.coefficient
