# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple, Optional
import torch
import numpy as np
from d3m_metadata.sequence import ndarray
from primitive_interfaces.base import PrimitiveBase, CallMetadata
from ..helpers import remove_mean
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple(
    'Params', [('transformation', ndarray), ('n_components', int)])


class PCA(PrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Example of a dimensionality reduction primitive.\n    '
    __author__ = 'William Harvey <willh@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Principal Component Analysis', 'algorithm_type': ['Dimensionality Reduction'], 'task_type': ['Feature Extraction'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [
    ], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'learning_type': ['Unsupervised Learning'], 'handles_regression': False, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False}

    def __init__(self, max_components=None, proportion_variance=1):
        # type: (int, float) -> None
        ' if both max_components and proportion_variance are set, the one which gives the\n            fewest principal components is used '
        super(PCA, self).__init__()
        self.max_components = max_components
        self.n_components = None  # type: Optional[int]
        self.proportion_variance = proportion_variance
        self.training_inputs = None  # type: Optional[torch.Variable]
        self.fitted = False
        self.transformation = None  # type: Optional[torch.Variable]
        self.mean = None

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        inputs = torch.from_numpy(np.array(inputs)).type(torch.DoubleTensor)
        return np.array([torch.mv(self.transformation, (row - self.mean)).numpy() for row in inputs])

    def set_training_data(self, inputs, outputs=None):
        # type: (Inputs, None) -> None
        self.training_inputs = torch.from_numpy(
            inputs).type(torch.DoubleTensor)
        self.fitted = False
        self.n_components = None

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if (self.training_inputs is None):
            raise ValueError('Missing training data.')
        (self.mean, zero_mean_data) = remove_mean(self.training_inputs)
        cov = torch.from_numpy(np.cov(self.training_inputs.numpy().T))
        cov = cov.type(torch.FloatTensor)
        (e, V) = torch.eig(cov, True)
        total_variance = sum(
            (np.linalg.norm(e.numpy()[i, :]) for i in range(e.size()[0])))
        indices = []
        self.n_components = 0
        recovered_variance = 0
        while ((recovered_variance < (self.proportion_variance * total_variance)) and ((self.max_components is None) or (self.n_components < self.max_components))):
            best_index = max(range(e.size()[0]), key=(
                lambda x: np.linalg.norm(e.numpy()[x, :])))
            indices.append(best_index)
            recovered_variance += np.linalg.norm(e.numpy()[best_index, :])
            e[best_index, :] = torch.zeros(2)
            self.n_components += 1
        self.transformation = torch.zeros(
            [self.n_components, self.training_inputs.size()[1]]).type(torch.DoubleTensor)
        for (n, indice) in enumerate(indices):
            self.transformation[n, :] = V[:, indice]
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(transformation=self.transformation.numpy(), n_components=self.n_components)

    def set_params(self, params):
        # type: (Params) -> None
        self.transformation = torch.from_numpy(
            params.transformation).type(torch.DoubleTensor)

    def get_call_metadata(self):
        # type: () -> CallMetadata
        return CallMetadata(has_finished=self.fitted, iterations_done=None)
