# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple, Union, List, Sequence, Any
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = Sequence[Any]
Params = NamedTuple('Params', [('estimators', List[DecisionTreeClassifier])])


class RandomForest(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Example of a primitive wrapping an existing sklearn classifier.\n    '
    __author__ = 'Oxford DARPA D3M "Hasty" team'
    __metadata__ = {'common_name': 'Random Forest', 'algorithm_type': ['Ensemble', 'Decision Tree'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': False, 'handles_classification': True, 'handles_multiclass': True, 'handles_multilabel': True, 'input_type': ['DENSE', 'UNISGNED_DATA'], 'output_type': ['PREDICTIONS']}

    def __init__(self, n_estimators=10, criterion='gini', max_features='auto', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_leaf_nodes=None, min_impurity_decrease=0.0, oob_score=False):
        # type: (int, str, Union[(int, str)], int, int, int, float, int, float, bool) -> None
        super(RandomForest, self).__init__()
        self.random_forest = RandomForestClassifier(n_estimators=n_estimators, criterion=criterion, max_features=max_features, max_depth=max_depth, min_samples_split=min_samples_split,
                                                    min_samples_leaf=min_samples_leaf, min_weight_fraction_leaf=min_weight_fraction_leaf, max_leaf_nodes=max_leaf_nodes, min_impurity_decrease=min_impurity_decrease, oob_score=oob_score)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: Sequence[Any]
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.random_forest.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.random_forest.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(estimators=self.random_forest.estimators_)

    def set_params(self, params):
        # type: (Params) -> None
        self.random_forest.estimators_ = params.estimators
