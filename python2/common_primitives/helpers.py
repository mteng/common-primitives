# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import math
from typing import *
import torch
from torch.autograd import Variable
import numpy as np
import d3m_metadata


def add_dicts(dict1, dict2):
    summation = {}
    for key in dict1:
        summation[key] = (dict1[key] + dict2[key])
    return summation


def sum_dicts(dictArray):
    assert (len(dictArray) > 0)
    summation = dictArray[0]
    for dictionary in dictArray:
        summation = add_dicts(summation, dictionary)
    return summation


def to_variable(value, requires_grad=False):
    # type: (Any) -> Variable
    '\n    Converts an input to torch Variable object\n    input\n    -----\n    value - Type: scalar, Variable object, torch.Tensor, numpy ndarray\n    requires_grad  - Type: bool . If true then we require the gradient of that object\n\n    output\n    ------\n    torch.autograd.variable.Variable object\n    '
    if isinstance(value, Variable):
        return value
    elif torch.is_tensor(value):
        return Variable(value.float(), requires_grad=requires_grad)
    elif isinstance(value, np.ndarray):
        return Variable(torch.from_numpy(value.astype(float)).float(), requires_grad=requires_grad)
    elif isinstance(value, d3m_metadata.sequence.ndarray):
        return Variable(torch.from_numpy(value.astype(float)).float(), requires_grad=requires_grad)
    else:
        return Variable(torch.Tensor([float(value)]), requires_grad=requires_grad)


def to_tensor(value):
    # type: (Any) -> torch.FloatTensor
    '\n    Converts an input to a torch FloatTensor\n    '
    if isinstance(value, np.ndarray):
        return torch.from_numpy(value).float()
    else:
        raise ValueError('Unsupported type: {}'.format(type(value)))


def refresh_node(node):
    return torch.autograd.Variable(node.data, True)


def log_mvn_likelihood(mean, covariance, observation):
    '\n    all torch primitives\n    all non-diagonal elements of covariance matrix are assumed to be zero\n    '
    k = mean.size()[0]
    variances = covariance.diag()
    log_likelihood = 0
    for i in range(k):
        log_likelihood += ((((- 0.5) * torch.log(variances[i])) - ((0.5 * k) * math.log(
            (2 * math.pi)))) - (0.5 * (((observation[i] - mean[i]) ** 2) / variances[i])))
    return log_likelihood


def covariance(data):
    '\n    input: NxD torch array\n    output: DxD torch array\n\n    calculates covariance matrix of input\n    '
    (N, D) = data.size()
    cov = torch.zeros([D, D]).type(torch.DoubleTensor)
    for contribution in ((torch.matmul(row.view(D, 1), row.view(1, D)) / N) for row in data):
        cov += contribution
    return cov


def remove_mean(data):
    '\n    input: NxD torch array\n    output: D-length mean vector, NxD torch array\n\n    takes a torch tensor, calculates the mean of each\n    column and subtracts it\n\n    returns (mean, zero_mean_data)\n    '
    (N, D) = data.size()
    mean = torch.zeros([D]).type(torch.DoubleTensor)
    for row in data:
        mean += (row.view(D) / N)
    zero_mean_data = (data - mean.view(1, D).expand(N, D))
    return (mean, zero_mean_data)


def denumpify(unknown_object):
    "\n    changes 'numpy.int's and 'numpy.float's etc to standard Python equivalents\n    no effect on other data types\n    "
    try:
        return unknown_object.item()
    except AttributeError:
        return unknown_object
