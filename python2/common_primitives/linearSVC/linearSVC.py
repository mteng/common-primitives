# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple
from sklearn.svm import LinearSVC as LSVC
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('coefficient', ndarray)])


class LinearSVC(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Primitive wrapping for sklearn.svm.LinearSVC.\n    '
    __author__ = 'NYU DARPA D3M team'
    __metadata__ = {'common_name': 'Linear Support Vector Classification', 'algorithm_type': ['Classification'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': False, 'handles_classification': True, 'handles_multiclass': True, 'handles_multilabel': False, 'input_type': ['DENSE', 'SPARSE'], 'output_type': ['PREDICTIONS']}

    def __init__(self, penalty='l2', loss='squared_hinge', dual=True, tol=0.0001, C=1.0, multi_class='ovr', fit_intercept=True, intercept_scaling=1, class_weight=None, _verbose=0, max_iter=1000):
        # type: (str, str, bool, float, float, str, bool, float, dict, int, int) -> None
        super(LinearSVC, self).__init__()
        self.linear_svc = LSVC(penalty=penalty, loss=loss, dual=dual, tol=tol, C=C, multi_class=multi_class, fit_intercept=fit_intercept,
                               intercept_scaling=intercept_scaling, class_weight=class_weight, verbose=_verbose, max_iter=max_iter)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.linear_svc.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.linear_svc.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(coefficient=self.linear_svc.coef_)

    def set_params(self, params):
        # type: (Params) -> None
        self.linear_svc.coef_ = params.coefficient
