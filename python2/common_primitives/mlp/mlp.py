# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import Tuple
try:
    from functools import reduce
except ImportError:
    from six.moves import reduce as reduce
from operator import mul
import torch.nn as nn
from ..nn_primitive import NNPrimitive


class MLP(NNPrimitive):
    __author__ = 'Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Multi-layer Perceptron', 'algorithm_type': ['Deep Learning'], 'compute_resources': {'sample_size': [
    ], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}}

    def __init__(self, loss='mse', output_shape=None, linear_output=False, depth=2, width=128, activation='relu'):
        # type: (str, Tuple[(int, ...)], bool, int, int, str) -> None
        super(MLP, self).__init__(
            loss=loss, output_shape=output_shape, linear_output=linear_output)
        self._depth = depth
        self._width = width
        self._activation = activation

    def _init_net(self):

        class _Net(nn.Module):

            def __init__(self, input_shape, output_shape, depth, width, activation, linear_output):
                super(_Net, self).__init__()
                self._input_dim = reduce(mul, input_shape)
                self._output_dim = reduce(mul, output_shape)
                if (activation == 'relu'):
                    self._activation = nn.ReLU()
                elif (activation == 'tanh'):
                    self._activation = nn.Tanh()
                elif (activation == 'sigmoid'):
                    self._activation = nn.Sigmoid()
                else:
                    raise ValueError(
                        'Unsupported activation type: {}. Available options: relu, tanh, sigmoid'.format(activation))
                self._linear_output = linear_output
                self._layers = []
                if (depth == 1):
                    self._layers.append(
                        nn.Linear(self._input_dim, self._output_dim))
                elif (depth > 1):
                    self._layers.append(nn.Linear(self._input_dim, width))
                    for i in range((depth - 1)):
                        if (i == (depth - 2)):
                            self._layers.append(
                                nn.Linear(width, self._output_dim))
                        else:
                            self._layers.append(nn.Linear(width, width))
                else:
                    raise ValueError(
                        'Depth must be greater than or equal to one.')
                for i in range(len(self._layers)):
                    nn.init.xavier_uniform(
                        self._layers[i].weight, gain=nn.init.calculate_gain(activation))
                    self.add_module('_layers[{0}]'.format(i), self._layers[i])

            def forward(self, x):
                x = x.view((- 1), self._input_dim)
                for i in range(len(self._layers)):
                    layer = self._layers[i]
                    x = layer(x)
                    if (self._linear_output and (i == (len(self._layers) - 1))):
                        return x
                    else:
                        x = self._activation(x)
                return x
        self._net = _Net(self._input_shape, self._output_shape, self._depth,
                         self._width, self._activation, self._linear_output)
