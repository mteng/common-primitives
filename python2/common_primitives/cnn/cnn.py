# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import Tuple
try:
    from functools import reduce
except ImportError:
    from six.moves import reduce as reduce
from operator import mul
import torch
import torch.nn as nn
import torch.nn.functional as F
from ..helpers import to_variable
from ..nn_primitive import NNPrimitive


class CNN(NNPrimitive):
    __author__ = 'Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Convolutional neural network', 'algorithm_type': ['Deep Learning'], 'compute_resources': {'sample_size': [
    ], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}}

    def __init__(self, output_shape=None):
        # type: (Tuple[(int, ...)]) -> None
        super(CNN, self).__init__(
            loss='crossentropy', output_shape=output_shape)

    def _init_net(self):

        class _Net(nn.Module):

            def __init__(self, input_shape, output_shape):
                super(_Net, self).__init__()
                self._input_shape = input_shape
                self._output_dim = reduce(mul, output_shape)
                self.conv1 = nn.Conv2d(1, 64, kernel_size=3)
                self.conv2 = nn.Conv2d(64, 128, kernel_size=3)

            def configure(self):
                input_sample = to_variable(
                    torch.zeros(self._input_shape).unsqueeze(0))
                self._cnn_output_dim = self._forward_cnn(
                    input_sample).view((- 1)).size(0)
                self.fc1 = nn.Linear(self._cnn_output_dim, 50)
                self.fc2 = nn.Linear(50, self._output_dim)

            def _forward_cnn(self, x):
                if (x.dim() == 3):
                    x = x.unsqueeze(1)
                x = F.relu(F.max_pool2d(self.conv1(x), 2))
                x = F.relu(F.max_pool2d(self.conv2(x), 2))
                return x

            def forward(self, x):
                x = self._forward_cnn(x)
                x = x.view((- 1), self._cnn_output_dim)
                x = F.relu(self.fc1(x))
                x = F.dropout(x)
                x = self.fc2(x)
                return x
        self._net = _Net(self._input_shape, self._output_shape)
        self._net.configure()
