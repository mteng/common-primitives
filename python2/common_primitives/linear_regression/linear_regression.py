# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import time
from typing import NamedTuple, Sequence, Any, Tuple
import torch
import numpy as np
from d3m_metadata.sequence import ndarray
from primitive_interfaces.base import ProbabilisticCompositionalityMixin, GradientCompositionalityMixin, SamplingCompositionalityMixin, ContinueFitMixin, CallMetadata
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from ..helpers import to_variable, refresh_node, log_mvn_likelihood
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple(
    'Params', [('weights', ndarray), ('offset', float), ('noise_variance', float)])
Scores = NamedTuple(
    'Scores', [('weights', ndarray), ('offset', float), ('noise_variance', float)])
Gradients = float


class LinearRegression(ProbabilisticCompositionalityMixin[(Inputs, Outputs, Params)], GradientCompositionalityMixin[(Inputs, Outputs, Params)], SamplingCompositionalityMixin[(Inputs, Outputs, Params)], ContinueFitMixin[(Inputs, Outputs, Params)], SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Example of a primitive wrapping implementing a linear regression using a high-performance\n    PyTorch backend, providing methods from multiple mixins.\n    '
    __author__ = 'William Harvey <willh@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Linear Regression', 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': [
    ]}, 'handles_regression': True, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False, 'algorithm_type': ['Gradient Descent'], 'input_type': ['DENSE'], 'output_type': ['PREDICTIONS'], 'task_type': ['Modeling'], 'learning_type': ['Supervised learning']}

    def __init__(self, alpha=0.001, beta=1e-08):
        # type: (float, float) -> None
        "\n        Optionally set ``alpha``, the initial fitting step size,\n        and ``beta``, which is as described in Baydin, Atilim Gunes, et al.,\n        'Online Learning Rate Adaptation with Hypergradient Descent',\n        arXiv preprint arXiv:1703.04782 (2017).\n        "
        super(LinearRegression, self).__init__()
        self.alpha = self.initial_alpha = alpha
        self.beta = beta
        self.fit_term_temperature = 0.0
        self.offset = None  # type: torch.autograd.Variable
        self.weights = None  # type: torch.autograd.Variable
        self.noise_variance = None  # type: torch.autograd.Variable
        self.training_inputs = None  # type: torch.autograd.Variable
        self.training_outputs = None  # type: torch.autograd.Variable
        self.iterations_done = None  # type: int
        self.has_finished = True
        self.new_training_data = True

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = to_variable(inputs, requires_grad=True)
        self.training_outputs = to_variable(outputs, requires_grad=True)
        self.new_training_data = True

    def _produce_one(self, input):
        # type: (ndarray) -> float
        return float((self.offset + torch.dot(self.weights, to_variable(input))).data.numpy()[0])

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        '\n        inputs : num_inputs x D numpy array\n        outputs : numpy array of dimension (num_inputs)\n        '
        self.iterations_done = None
        self.has_finished = True
        return np.array([self._produce_one(input) for input in inputs])

    def continue_fit(self, timeout=None, iterations=None, weights_prior=None, noise_prior=None, offset_prior=None, fit_threshold=0, batch_size=None):
        # type: (float, int, GradientCompositionalityMixin[(Any, ndarray, Any)], GradientCompositionalityMixin[(Any, Sequence[float], Any)], GradientCompositionalityMixin[(Any, Sequence[float], Any)], float, int) -> None
        '\n        Starting from previous parameters, runs gradient descent for ``timeout``\n        seconds or ``iterations`` iterations, whichever comes sooner, on log\n        normal_density(self.weights * self.input - output, identity*self.noise_variance) +\n        parameter_prior_primitives["weights"].score(self.weights) +\n        parameter_prior_primitives["noise_variance"].score(noise_variance).\n        '
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        return self._fit(timeout=timeout, iterations=iterations, weights_prior=weights_prior, noise_prior=noise_prior, fit_threshold=fit_threshold, batch_size=batch_size)

    def fit(self, timeout=None, iterations=None, weights_prior=None, noise_prior=None, offset_prior=None, fit_threshold=0, batch_size=None):
        # type: (float, int, GradientCompositionalityMixin[(Any, ndarray, Any)], GradientCompositionalityMixin[(Any, Sequence[float], Any)], GradientCompositionalityMixin[(Any, Sequence[float], Any)], float, int) -> None
        '\n        Runs gradient descent for ``timeout`` seconds or ``iterations``\n        iterations, whichever comes sooner, on log normal_density(self.weights * self.input\n        - output, identity*self.noise_variance) +\n        parameter_prior_primitives["weights"].score(self.weights) +\n        parameter_prior_primitives["noise_variance"].score(noise_variance).\n        '
        if self.new_training_data:
            self.weights = torch.autograd.Variable(torch.FloatTensor(
                (np.random.randn(self.training_inputs.size()[1]) * 0.001)), requires_grad=True)
            self.noise_variance = torch.autograd.Variable(
                torch.ones(1), requires_grad=True)
            self.offset = torch.autograd.Variable(
                torch.zeros(1), requires_grad=True)
            self.new_training_data = False
            self.alpha = self.initial_alpha
        return self._fit(timeout=timeout, iterations=iterations, weights_prior=weights_prior, noise_prior=noise_prior, fit_threshold=fit_threshold, batch_size=batch_size)

    def _fit(self, batch_size, timeout=None, iterations=None, weights_prior=None, noise_prior=None, offset_prior=None, fit_threshold=0):
        # type: (int, float, int, GradientCompositionalityMixin[(Any, ndarray, Any)], GradientCompositionalityMixin[(Any, Sequence[float], Any)], GradientCompositionalityMixin[(Any, Sequence[float], Any)], float) -> None
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        if (timeout is None):
            timeout = np.inf
        if (batch_size is None):
            x_batches = [self.training_inputs]
            y_batches = [self.training_outputs]
        else:
            x_batches = []
            y_batches = []
            for i in range(0, len(self.training_inputs), batch_size):
                x_batches.append(self.training_inputs[i:(i + batch_size)])
                y_batches.append(self.training_outputs[i:(i + batch_size)])
        num_batches = len(x_batches)
        start = time.time()
        self.iterations_done = 0
        self.has_finished = False
        (prev_weights_grad, prev_noise_grad, prev_offset_grad) = (None, None, None)
        while ((time.time() < (start + timeout)) and (((iterations is None) and (self.alpha >= fit_threshold)) or (self.iterations_done < iterations))):
            self.iterations_done += 1
            batch_no = (self.iterations_done % num_batches)
            grads = [self._gradient_params_log_likelihood(input=training_input, output=training_output) for (
                training_input, training_output) in zip(x_batches[batch_no], y_batches[batch_no])]
            weights_grad = (sum((grad[0] for grad in grads)) * num_batches)
            offset_grad = (sum((grad[1] for grad in grads)) * num_batches)
            noise_grad = (sum((grad[2] for grad in grads)) * num_batches)
            if (weights_prior is not None):
                weights_grad += torch.from_numpy(weights_prior.gradient_output(
                    outputs=np.array([self.weights.data.numpy()]), inputs=[]))
            if (offset_prior is not None):
                prev_offset_grad += torch.from_numpy(offset_prior.gradient_output(
                    outputs=[self.offset.data.numpy()], inputs=[]))
            if (noise_prior is not None):
                noise_grad += torch.from_numpy(noise_prior.gradient_output(
                    outputs=[self.noise_variance.data.numpy()], inputs=[]))
            if (prev_weights_grad is not None):
                self.alpha += (self.beta * ((torch.dot(weights_grad, prev_weights_grad) + torch.dot(
                    noise_grad, prev_noise_grad)) + torch.dot(offset_grad, prev_offset_grad)))
            prev_weights_grad = weights_grad
            prev_offset_grad = offset_grad
            prev_noise_grad = noise_grad
            self.weights.data += ((weights_grad * self.alpha) /
                                  torch.norm(weights_grad))
            self.offset.data += ((offset_grad * self.alpha) /
                                 torch.norm(offset_grad))
            self.noise_variance.data += ((noise_grad *
                                          self.alpha) / torch.norm(noise_grad))
            self.weights = refresh_node(self.weights)
            self.offset = refresh_node(self.offset)
            self.noise_variance = refresh_node(self.noise_variance)

    def get_params(self):
        # type: () -> Params
        return Params(weights=self.weights.data.numpy(), offset=self.offset.data.numpy(), noise_variance=self.noise_variance.data.numpy())

    def set_params(self, params):
        # type: (Params) -> None
        self.weights = to_variable(params.weights, requires_grad=True)
        self.weights.retain_grad()
        self.noise_variance = to_variable(
            params.noise_variance, requires_grad=True)
        self.offset = to_variable(params.offset, requires_grad=True)

    def log_likelihood(self, outputs, inputs):
        # type: (Outputs, Inputs) -> float
        '\n        input : D-length numpy ndarray\n        output : float\n        Calculates\n        log(normal_density(self.weights * self.input - output, identity * self.noise_variance))\n        for a single input/output pair.\n        '
        return float(sum((self._log_likelihood(output=to_variable(output), input=to_variable(input)).data.numpy() for (input, output) in zip(inputs, outputs)))[0])

    def _log_likelihood(self, output, input):
        # type: (torch.autograd.Variable, torch.autograd.Variable) -> torch.autograd.Variable
        '\n        All inputs are torch tensors (or variables if grad desired).\n        input : D-length torch to_variable\n        output : float\n        '
        expected_output = (torch.dot(self.weights, input) + self.offset)
        covariance = to_variable(self.noise_variance).view(1, 1)
        return log_mvn_likelihood(expected_output, covariance, output)

    def _gradient_params_log_likelihood(self, output, input):
        # type: (torch.autograd.Variable, torch.autograd.Variable) -> Tuple[(torch.autograd.Variable, torch.autograd.Variable, torch.autograd.Variable)]
        '\n        Output is ( D-length torch variable, 1-length torch variable )\n        '
        self.weights = refresh_node(self.weights)
        self.noise_variance = refresh_node(self.noise_variance)
        log_likelihood = self._log_likelihood(output=output, input=input)
        log_likelihood.backward()
        return (self.weights.grad.data, self.offset.grad.data, self.noise_variance.grad.data)

    def _gradient_output_log_likelihood(self, output, input):
        # type: (ndarray, torch.autograd.Variable) -> torch.autograd.Variable
        '\n        output is D-length torch variable\n        '
        output_var = to_variable(output)
        log_likelihood = self._log_likelihood(output=output_var, input=input)
        log_likelihood.backward()
        return output_var.grad

    def gradient_output(self, outputs, inputs):
        # type: (Outputs, Inputs) -> Gradients
        '\n        Calculates grad_output fit_term_temperature *\n        log normal_density(self.weights * self.input - output, identity * self.noise_variance)\n        for a single input/output pair.\n        '
        outputs_vars = [to_variable(output, requires_grad=True)
                        for output in outputs]
        inputs_vars = [to_variable(input) for input in inputs]
        grad = sum((self._gradient_output_log_likelihood(output=output, input=input)
                    for (input, output) in zip(inputs_vars, outputs_vars)))
        if (self.fit_term_temperature != 0):
            grad += (self.fit_term_temperature * sum((self._gradient_output_log_likelihood(output=training_output, input=training_input)
                                                      for (training_output, training_input) in zip(self.training_outputs, self.training_inputs))))
        return float(grad.data.numpy()[0])

    def gradient_params(self, outputs, inputs):
        # type: (Outputs, Inputs) -> Scores
        '\n        Calculates grad_weights fit_term_temperature *\n        log normal_density(self.weights * self.input - output, identity * self.noise_variance)\n        for a single input/output pair.\n        '
        outputs_vars = [to_variable(output, requires_grad=True)
                        for output in outputs]
        inputs_vars = [to_variable(input) for input in inputs]
        grads = [self._gradient_params_log_likelihood(output=output, input=input) for (
            input, output) in zip(inputs_vars, outputs_vars)]
        grad_weights = sum((grad[0] for grad in grads))
        grad_offset = sum((grad[1] for grad in grads))
        grad_noise_variance = sum((grad[2] for grad in grads))
        if (self.fit_term_temperature != 0):
            training_grads = [self._gradient_params_log_likelihood(output=output, input=input) for (
                input, output) in zip(self.training_inputs, self.training_outputs)]
            grad_weights += (self.fit_term_temperature *
                             sum((grad[0] for grad in training_grads)))
            grad_offset += (self.fit_term_temperature *
                            sum((grad[1] for grad in training_grads)))
            grad_noise_variance += (self.fit_term_temperature *
                                    sum((grad[2] for grad in training_grads)))
        return Scores(weights=grad_weights, offset=grad_offset, noise_variance=grad_noise_variance)

    def set_fit_term_temperature(self, temperature=0):
        # type: (float) -> None
        self.fit_term_temperature = temperature

    def _sample_once(self, inputs):
        # type: (Inputs) -> Outputs
        '\n        input : NxD numpy ndarray\n        outputs : N-length numpy ndarray\n        '
        if ((self.weights is None) or (self.noise_variance is None)):
            raise ValueError('Params not set.')
        output_means = [(np.dot(self.weights.data.numpy(), input) +
                         self.offset.data.numpy()) for input in inputs]
        outputs = np.random.normal(output_means, self.noise_variance.data[0])
        return outputs

    def sample(self, inputs, num_samples=1, timeout=None, iterations=None):
        # type: (Inputs, int, float, int) -> Sequence[Outputs]
        '\n        input : num_inputs x D numpy ndarray\n        outputs : num_predictions x num_inputs numpy ndarray\n        '
        return [self._sample_once(inputs=inputs) for _ in range(num_samples)]

    def get_call_metadata(self):
        # type: () -> CallMetadata
        return CallMetadata(has_finished=self.has_finished, iterations_done=self.iterations_done)
