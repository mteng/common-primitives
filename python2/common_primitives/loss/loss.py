# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import Tuple
from d3m_metadata.sequence import ndarray
from primitive_interfaces.base import GradientCompositionalityMixin
from primitive_interfaces.transformer import TransformerPrimitiveBase
import torch
import torch.nn as nn
from ..helpers import to_variable
Void = type(None)
Inputs = ndarray
Outputs = float
Params = Void


class Loss(TransformerPrimitiveBase[(Inputs, Outputs)], GradientCompositionalityMixin[(Inputs, Outputs, Params)]):
    __author__ = 'Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    __metadata__ = {'team': 'Oxford DARPA D3M "Hasty" team', 'common_name': 'Loss primitive', 'algorithm_type': [''], 'compute_resources': {'sample_size': [], 'sample_unit': [
    ], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}}

    def __init__(self, target_outputs, loss='mse'):
        # type: (Inputs, str) -> None
        super(Loss, self).__init__()
        if (loss == 'l1'):
            self._criterion = nn.L1Loss()
        elif (loss == 'mse'):
            self._criterion = nn.MSELoss()
        elif (loss == 'crossentropy'):
            self._criterion = nn.CrossEntropyLoss()
        else:
            raise ValueError(
                'Unsupported loss: {}. Available options: l1, mse, crossentropy'.format(loss))
        self._actual_outputs = None  # type: torch.Variable
        self._target_outputs = to_variable(target_outputs)
        self._loss = None  # type: torch.Variable

    def produce(self, actual_outputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        self._actual_outputs = to_variable(actual_outputs, requires_grad=True)
        self._loss = self._criterion(
            self._actual_outputs, self._target_outputs)
        return self._loss.data[0]

    def backprop(self, gradient_output=1):
        # type: (Outputs) -> Tuple[(Inputs, None)]
        if (self._loss is None):
            raise Exception(
                'Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            if (self._actual_outputs.grad is not None):
                self._actual_outputs.grad.data.zero_()
            self._loss.backward(gradient=to_variable(gradient_output))
            return (self._actual_outputs.grad, None)

    def gradient_output(self):
        raise NotImplementedError()

    def gradient_params(self):
        raise NotImplementedError()

    def set_fit_term_temperature(self):
        raise NotImplementedError()
