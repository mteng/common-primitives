# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple
from sklearn.linear_model import BayesianRidge as BRR
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = ndarray
Params = NamedTuple('Params', [('coefficient', ndarray)])


class BayesianRidge(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Primitive wrapping for sklearn.linear_model.BayesianRidge.\n    '
    __author__ = 'NYU DARPA D3M team'
    __metadata__ = {'common_name': 'Bayesian Ridge Regression', 'algorithm_type': ['Bayesian', 'Regression'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': True, 'handles_classification': False, 'handles_multiclass': False, 'handles_multilabel': False, 'input_type': ['DENSE', 'SPARSE'], 'output_type': ['PREDICTIONS']}

    def __init__(self, n_iter=300, tol=0.001, alpha_1=1e-06, alpha_2=1e-06, lambda_1=1e-06, lambda_2=1e-06, compute_score=False, fit_intercept=True, normalize=False, _verbose=False):
        # type: (int, float, float, float, float, float, bool, bool, bool, bool) -> None
        super(BayesianRidge, self).__init__()
        self.bayesian_ridge = BRR(n_iter=n_iter, tol=tol, alpha_1=alpha_1, alpha_2=alpha_2, lambda_1=lambda_1, lambda_2=lambda_2,
                                  compute_score=compute_score, fit_intercept=fit_intercept, normalize=normalize, copy_X=True, verbose=_verbose)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.bayesian_ridge.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.bayesian_ridge.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(coefficient=self.bayesian_ridge.coef_)

    def set_params(self, params):
        # type: (Params) -> None
        self.bayesian_ridge.coef_ = params.coefficient
