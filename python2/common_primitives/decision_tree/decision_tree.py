# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from typing import NamedTuple, Sequence, Any
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree._tree import Tree
from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
Inputs = ndarray
Outputs = Sequence[Any]
Params = NamedTuple('Params', [('tree', Tree)])


class DecisionTree(SupervisedLearnerPrimitiveBase[(Inputs, Outputs, Params)]):
    '\n    Primitive wrapping sklearn.tree.DecisionTreeClassifier.\n    '
    __author__ = 'NYU DARPA D3M team'
    __metadata__ = {'common_name': 'Decision Tree', 'algorithm_type': ['Decision Tree', 'Classification'], 'compute_resources': {'sample_size': [], 'sample_unit': [], 'disk_per_node': [], 'expected_running_time': [], 'gpus_per_node': [], 'cores_per_node': [
    ], 'mem_per_gpu': [], 'mem_per_node': [], 'num_nodes': []}, 'handles_regression': False, 'handles_classification': True, 'handles_multiclass': True, 'handles_multilabel': True, 'input_type': ['DENSE', 'UNISGNED_DATA'], 'output_type': ['PREDICTIONS']}

    def __init__(self, criterion='gini', splitter='best', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features=None, max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, class_weight=None, presort=False):
        # type: (str, str, int, int, int, float, int, int, float, float, dict, bool) -> None
        super(DecisionTree, self).__init__()
        self.decision_tree = DecisionTreeClassifier(criterion=criterion, splitter=splitter, max_depth=max_depth, min_samples_split=min_samples_split, min_samples_leaf=min_samples_leaf, min_weight_fraction_leaf=min_weight_fraction_leaf,
                                                    max_features=max_features, max_leaf_nodes=max_leaf_nodes, min_impurity_decrease=min_impurity_decrease, min_impurity_split=min_impurity_split, class_weight=class_weight, presort=presort)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: Sequence[Any]
        self.fitted = False

    def produce(self, inputs, timeout=None, iterations=None):
        # type: (Inputs, float, int) -> Outputs
        return self.decision_tree.predict(inputs)

    def set_training_data(self, inputs, outputs):
        # type: (Inputs, Outputs) -> None
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, timeout=None, iterations=None):
        # type: (float, int) -> None
        if self.fitted:
            return
        if ((self.training_inputs is None) or (self.training_outputs is None)):
            raise ValueError('Missing training data.')
        self.decision_tree.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self):
        # type: () -> Params
        return Params(tree=self.decision_tree.tree_)

    def set_params(self, params):
        # type: (Params) -> None
        self.decision_tree.tree_ = params.tree
