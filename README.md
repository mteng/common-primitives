# Common TA1 primitives

A common set of TA1 primitives using the [primitive interfaces](https://gitlab.datadrivendiscovery.org/d3m/primitive-interfaces).

## Installation

This package works on both Python 2.7 and Python 3.5+.

Besides requirements you have to first install [PyTorch](http://pytorch.org/). It is just
used in some primitives as an example, but it cannot be installed directly from PyPI.

After that you can run
```
pip install -r requirements.txt
pip install .
```
in the root directory of this repository to install the `common_primitives` package.

## Changelog

See [HISTORY.md](./HISTORY.md) for summary of changes to this package.

## Running

Each primitive comes with simple test code which you can run like
```
python -m common_primitives.random_forest.random_forest
```

If you use [d3m package](https://gitlab.datadrivendiscovery.org/d3m/d3m), these common primitives also register
themselves under the D3M namespace. So `common_primitives.random_forest.random_forest.RandomForest` is available under
`d3m.primitives.random_forest.RandomForest` as well:

```python
import numpy as np
from d3m.primitives import random_forest
a = random_forest.RandomForest()
train_y = np.array(['a', 'b', 'a', 'b', 'a'])
train_x = np.array([[2, 8], [3.5, 3], [7, 2], [8, 1], [9, 13]])
a.set_training_data(inputs=train_x, outputs=train_y)
a.fit()
print(a.produce(inputs=np.array([[3, 3], [13, 13]])))
```

## Contribute

Feel free to contribute more primitives to this repository. The idea is that we build
a common set of primitives which can help both as an example, but also to have shared
maintenance of some primitives, especially glue primitives and wrapped sklearn primitives.

All primitives are written in Python 3 and are type checked using
[mypy](http://www.mypy-lang.org/), so typing annotations are required.
Python 3 is then converted automatically to Python 2.7.
