import os
import re
import sys
from setuptools import setup, find_packages

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

PACKAGE_NAME = 'common_primitives'

if sys.version_info.major == 2:
    MINIMUM_PYTHON_VERSION = 2, 7
else:
    MINIMUM_PYTHON_VERSION = 3, 5

REQUIREMENTS_MAP = {
    'metadata': 'd3m_metadata',
}


def check_python_version():
    """Exit when the Python version is too low."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        sys.exit("Python {}.{}+ is required.".format(*MINIMUM_PYTHON_VERSION))


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert False, "'{0}' not found in '{1}'".format(key, module_path)


def package_from_requirement(requirement):
    """Convert pip requirement string to a package name."""
    if requirement.startswith('git+'):
        requirement_path = urlparse(requirement).path
        cleaned_repository_name = re.sub(r'-', r'_', re.sub(r'\.git$', r'', requirement_path.split('/')[-1]))
        if cleaned_repository_name in REQUIREMENTS_MAP:
            return REQUIREMENTS_MAP[cleaned_repository_name]
        else:
            return cleaned_repository_name
    else:
        return re.sub(r'\[[^\]]*\]', '', requirement)


def read_requirements():
    """Read the requirements."""
    with open('requirements.txt') as requirements:
        return [package_from_requirement(requirement.strip()) for requirement in requirements]

check_python_version()

package_dir = {2: 'python2', 3: '.'}[sys.version_info.major]

setup(
    name=PACKAGE_NAME,
    version=read_package_variable('__version__'),
    description='Common primitives maintained together',
    author=read_package_variable('__author__'),
    packages=find_packages(package_dir, exclude=['contrib', 'docs', 'tests*']),
    package_dir={'': package_dir},
    install_requires=read_requirements(),
    url='https://gitlab.com/datadrivendiscovery/common-primitives',
    entry_points = {
        'd3m.primitives': [
            'cnn.CNN = common_primitives.cnn.cnn:CNN',
            'csv_feature_extractor.CSVFeatureExtractor = common_primitives.csv_feature_extractor.csv_feature_extractor:CSVFeatureExtractor',
            'diagonal_mvn.DiagonalMVN = common_primitives.diagonal_mvn.diagonal_mvn:DiagonalMVN',
            'k_means.KMeans = common_primitives.k_means.k_means:KMeans',
            'linear_regression.LinearRegression = common_primitives.linear_regression.linear_regression:LinearRegression',
            'logistic_regression.BayesianLogisticRegression = common_primitives.logistic_regression.logistic_regression:BayesianLogisticRegression',
            'mlp.MLP = common_primitives.mlp.mlp:MLP',
            'one_hot_maker.OneHotMaker = common_primitives.one_hot_maker.one_hot_maker:OneHotMaker',
            'pca.PCA = common_primitives.pca.pca:PCA',
            'random_forest.RandomForest = common_primitives.random_forest.random_forest:RandomForest',
            'decision_tree.DecisionTree = common_primitives.decision_tree.decision_tree:DecisionTree',
            'linearSVC.LinearSVC = common_primitives.linearSVC.linearSVC:LinearSVC',
            'k_neighbors_classifier.KNeighborsClassifier = common_primitives.k_neighbors_classifier.k_neighbors_classifier:KNeighborsClassifier',
            'lassoCV.LassoCV = common_primitives.lassoCV.lassoCV:LassoCV',
            'bayesian_ridge.BayesianRidge = common_primitives.bayesian_ridge.bayesian_ridge:BayesianRidge',
            'ridge.Ridge = common_primitives.ridge.ridge:Ridge'
        ],
    },
)
